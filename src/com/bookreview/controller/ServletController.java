package com.bookreview.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookreview.dto.BookBeanDTO;
import com.bookreview.dto.ReviewsBeanDTO;
import com.bookreview.dto.UserBeanDTO;
import com.bookreview.service.AdminService;
import com.bookreview.service.CommonService;
import com.bookreview.service.UserService;
import com.bookreview.vo.BookBeanVO;
import com.bookreview.vo.ReviewsBeanVO;
import com.bookreview.vo.UserBeanVO;

public class ServletController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ServletController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		 PrintWriter out = response.getWriter();
		String uri = request.getRequestURI();

		if (uri.contains("addNewUser")) {
			String name = request.getParameter("name");
			String password = request.getParameter("password");

			UserBeanVO vo = new UserBeanVO();
			vo.setUserName(name);
			vo.setPassword(password);

			UserBeanDTO dto = new UserBeanDTO();
			dto.setUserName(vo.getUserName());

			UserService service = new UserService();
			int result = service.addNewUser(dto);

			out.println(result);
		}

		if (uri.contains("addBook")) {

			BookBeanVO nbvo = new BookBeanVO();
			nbvo.setIsbn(request.getParameter("isbn"));
			nbvo.setBookName(request.getParameter("bookName"));
			nbvo.setAuthorName(request.getParameter("authorName"));
			nbvo.setEdition(request.getParameter("edition"));
			nbvo.setPublisher(request.getParameter("publisher"));

			BookBeanDTO nbdto = new BookBeanDTO();
			nbdto.setAuthorName(nbvo.getAuthorName());
			nbdto.setBookName(nbvo.getBookName());
			nbdto.setEdition(Integer.parseInt(nbvo.getEdition()));
			nbdto.setIsbn(nbvo.getIsbn());
			nbdto.setPublisher(nbvo.getPublisher());

			AdminService service = new AdminService();
			boolean result = service.addNewBook(nbdto);

			out.println(result);
		}

		if (uri.contains("getBooks")) {
			CommonService service = new CommonService();
			String result = service.getBooks();
			out.println(result);
		}
		
		if (uri.contains("getReviews")) {
			BookBeanVO bvo = new BookBeanVO();
			bvo.setIsbn(request.getParameter("isbn"));

			CommonService service = new CommonService();
			ReviewsBeanVO vo=new ReviewsBeanVO();
			ReviewsBeanDTO dto=new ReviewsBeanDTO();
			String isbn=request.getParameter("isbn");
			vo.setIsbn(isbn);			
			dto.setIsbn(vo.getIsbn());
			String result = service.getReviews(dto);
			out.println(result);
		}
		
		if (uri.contains("validateLogin")) {
			UserBeanVO vo=new UserBeanVO();
			
			String userID=request.getParameter("userid");
			String pwd=request.getParameter("password");
			vo.setUserID(userID);
			vo.setPassword(pwd);
			
			
			UserBeanDTO dto=new UserBeanDTO();
			dto.setUserId(Integer.parseInt(vo.getUserID()));
			dto.setPassword(vo.getPassword());
			UserService service=new UserService();
			
			boolean result =service.validateUser(dto);
			out.println(result);
		}
		
		if(uri.contains("deleteBook")){
			BookBeanVO dbvo = new BookBeanVO();
			dbvo.setIsbn("isbn");
			BookBeanDTO dbdto = new BookBeanDTO();
			dbdto.setIsbn(dbvo.getIsbn());
			AdminService service = new AdminService();
			
			boolean result = service.deleteBook(dbdto);
			out.println(result);
		}
		if(uri.contains("addReview")){
			String comment=request.getParameter("comment");
			String isbn=request.getParameter("isbn");
			String userid=request.getParameter("userid");
			String ratings=request.getParameter("ratings");
			ReviewsBeanVO vo=new ReviewsBeanVO();
			vo.setComment(comment);
			vo.setIsbn(isbn);
			vo.setRatings(ratings);
			vo.setUserId(userid);
			ReviewsBeanDTO dto=new ReviewsBeanDTO();
			dto.setComment(vo.getComment());
			dto.setIsbn(vo.getIsbn());
			dto.setRatings(Integer.parseInt(vo.getRatings()));
			dto.setUserId(Integer.parseInt(vo.getUserId()));
			UserService service=new UserService();
			boolean result = service.addReview(dto);
			out.println(result);
		}
		if(uri.contains("updateReview")){
			String comment=request.getParameter("comment");
			String isbn=request.getParameter("isbn");
			String userid=request.getParameter("userid");
			String ratings=request.getParameter("ratings");
			ReviewsBeanVO vo=new ReviewsBeanVO();
			vo.setComment(comment);
			vo.setIsbn(isbn);
			vo.setRatings(ratings);
			vo.setUserId(userid);
			ReviewsBeanDTO dto=new ReviewsBeanDTO();
			dto.setComment(vo.getComment());
			dto.setIsbn(vo.getIsbn());
			dto.setRatings(Integer.parseInt(vo.getRatings()));
			dto.setUserId(Integer.parseInt(vo.getUserId()));
			UserService service=new UserService();
			boolean result = service.updateReview(dto);
			out.println(result);
		}
		

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
