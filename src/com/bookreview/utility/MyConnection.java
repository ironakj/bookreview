package com.bookreview.utility;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MyConnection {
	
	private MyConnection() {
	}
	
	public static Connection getConnection(){
		InputStream inputStream = MyConnection.class.getClassLoader().getResourceAsStream("/com/bookreview/cfgs/dbconfig.properties");
		Properties p = new Properties();
		Connection con = null;
		try {
			p.load(inputStream);
			String driver = p.getProperty("driver");
			String url = p.getProperty("url");
			String name = p.getProperty("username");
			String pwd = p.getProperty("password");
			Class.forName(driver);
			con = DriverManager.getConnection(url, name, pwd);
			
		} catch (IOException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
}
