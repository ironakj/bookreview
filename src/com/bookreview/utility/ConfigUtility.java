package com.bookreview.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigUtility {
	
	private ConfigUtility() {
}
	public static Properties getQueryProperties() throws IOException {
		InputStream inputStream = ConfigUtility.class.getClassLoader().getResourceAsStream("/com/bookreview/cfgs/query.properties");
		Properties properties = new Properties();
		properties.load(inputStream);
		return properties;
	}
}
