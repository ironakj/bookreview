package com.bookreview.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.bookreview.bo.BookBeanBO;
import com.bookreview.utility.ConfigUtility;
import com.bookreview.utility.MyConnection;
import java.sql.Statement;

public class BookDAOImpl implements BookDAO {
	Connection con = null;
	Properties properties = null;
	PreparedStatement query = null;
	Statement statement = null;
	ResultSet results = null;

	public boolean addNewBook(BookBeanBO nbbo) {
		try {
			properties = ConfigUtility.getQueryProperties();
			String newBook = properties.getProperty("$ADDBOOK");
			con = MyConnection.getConnection();
			query = con.prepareStatement(newBook);

			query.setString(1, nbbo.getIsbn());
			query.setString(2, nbbo.getBookName());
			query.setString(3, nbbo.getAuthorName());
			query.setInt(4, nbbo.getEdition());
			query.setString(5, nbbo.getPublisher());

			int status = query.executeUpdate();
			if (status != 0) {
				return true;
			}
		} catch (IOException | SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}

	public String getBooks() {
		try {
			properties = ConfigUtility.getQueryProperties();
			String getBooks = properties.getProperty("$GETBOOKS");
			con = MyConnection.getConnection();
			statement=con.createStatement();
			results = statement.executeQuery(getBooks);
			StringBuilder json = new StringBuilder("[");
			while (results.next()) {
				if(results.getRow() == 1){
					String book = "{\"isbn\":\"" + results.getString(1) + "\",\"bookName\":\"" + results.getString(2)+ "\",\"authorName\":\"" + results.getString(3) + "\",\"edition\":" + results.getInt(4)+ ",\"publisherName\":\"" + results.getString(5) + "\"}";
					json.append(book);
				}else{
					String book = ",{\"isbn\":\"" + results.getString(1) + "\",\"bookName\":\"" + results.getString(2)+ "\",\"authorName\":\"" + results.getString(3) + "\",\"edition\":" + results.getInt(4)+ ",\"publisherName\":\"" + results.getString(5) + "\"}";
					json.append(book);
				}
			}
			json.append("]");
			return json.toString();
		} catch (IOException | ClassNotFoundException | SQLException e) {
			//e.printStackTrace();
		}
		return "";
	}
	
	 public boolean deleteBook(BookBeanBO dbbo){
	    	try {
	    		
	    		properties = ConfigUtility.getQueryProperties();
				
				String deleteBook =properties.getProperty("$DELETEBOOK");
				
				con = MyConnection.getConnection();
				query = con.prepareStatement(deleteBook);
				
				query.setString(1, dbbo.getIsbn());
				
				int status = query.executeUpdate();
				query.close();
				if(status != 0)
				{
					return true;
				}
				
				

			} catch (IOException | SQLException | ClassNotFoundException e) {
				//e.printStackTrace();
			}

	    	return false;
	    }

}
