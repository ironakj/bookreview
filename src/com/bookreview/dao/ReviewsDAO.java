package com.bookreview.dao;

import com.bookreview.bo.ReviewsBeanBO;

public interface ReviewsDAO {
public boolean addReview(ReviewsBeanBO bo);
public boolean updateReview(ReviewsBeanBO bo);
public String getReviews(ReviewsBeanBO bo);
}
