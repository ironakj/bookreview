package com.bookreview.dao;

public class DAOFactory {
	private static UserDAO userDAOImpl;
	private static BookDAO bookDAOImpl;
	private static ReviewsDAO reviewsDAOImpl;
	
	private DAOFactory() {
	}

	

	public static UserDAO getUserInstance() {
		if (userDAOImpl == null) {
			return new UserDAOImpl();
		}
		return userDAOImpl;
	}

	public static BookDAO getBookInstance() {
		if (bookDAOImpl == null) {
			return new BookDAOImpl();
		}
		return bookDAOImpl;
	}
	
	public static ReviewsDAO getReviewsInstance() {

		if (reviewsDAOImpl == null) {
			return new ReviewsDAOImpl();
		}
		return reviewsDAOImpl;
	}
}
