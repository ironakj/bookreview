package com.bookreview.dao;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import com.bookreview.bo.UserBeanBO;
import com.bookreview.utility.ConfigUtility;
import com.bookreview.utility.MyConnection;

public class UserDAOImpl implements UserDAO {
	Connection con = null;
	Properties properties = null;
	CallableStatement cstuser= null;
	PreparedStatement pstcount = null;


	@Override
	public int createUser(UserBeanBO bo) {

		try {
			properties = ConfigUtility.getQueryProperties();
			String createUser = properties.getProperty("$CREATEUSER");
			String name = bo.getUserName();
			String role = bo.getRole();
			String pwd = bo.getPassword();

			con = MyConnection.getConnection();
			cstuser=con.prepareCall(createUser);
			cstuser.setString(1, name);
			cstuser.setString(2, role);
			cstuser.setString(3, pwd);
			cstuser.registerOutParameter(4, Types.INTEGER);
			cstuser.executeUpdate();
			return cstuser.getInt(4);
			

		} catch (ClassNotFoundException | IOException | SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int selectUser(UserBeanBO bo) {
		try {
			properties = ConfigUtility.getQueryProperties();
			String query1 = properties.getProperty("$VALIDATEUSER");
			pstcount=MyConnection.getConnection().prepareStatement(query1);
			pstcount.setString(1,bo.getUserName());
			pstcount.setString(2, bo.getPassword());
			ResultSet rs=pstcount.executeQuery();
			
			if(rs.next() && rs.getInt(1)!=0){
				return 1;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}

}
