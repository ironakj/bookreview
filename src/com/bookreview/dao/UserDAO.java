package com.bookreview.dao;

import com.bookreview.bo.UserBeanBO;

public interface UserDAO {
	public int createUser(UserBeanBO bo);
	public int selectUser(UserBeanBO bo);

}
