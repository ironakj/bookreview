package com.bookreview.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.bookreview.bo.ReviewsBeanBO;
import com.bookreview.utility.ConfigUtility;
import com.bookreview.utility.MyConnection;

public class ReviewsDAOImpl implements ReviewsDAO {
	Connection con = null;
	Properties properties = null;
	PreparedStatement query = null;
	Statement statement = null;
	ResultSet results = null;

	@Override
	public boolean addReview(ReviewsBeanBO bo) {
		try {
			properties = ConfigUtility.getQueryProperties();

			String newReview = properties.getProperty("$ADDREVIEW");

			con = MyConnection.getConnection();
			query = con.prepareStatement(newReview);
			query.setString(1, bo.getIsbn());
			query.setInt(2, bo.getUserID());
			query.setString(3, bo.getComment());

			int count = query.executeUpdate();

			if (count != 0) {
				return true;
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateReview(ReviewsBeanBO bo) {
		try {

			properties = ConfigUtility.getQueryProperties();

			String updatereview = properties.getProperty("$UPDATEREVIEW");
			con = MyConnection.getConnection();
			query = con.prepareStatement(updatereview);
			query.setString(1, bo.getComment());
			query.setInt(2, bo.getRatings());
			query.setInt(3, bo.getUserID());
			query.setString(4, bo.getIsbn());
			int count = query.executeUpdate();

			if (count != 0) {
				return true;
			}

		} catch (ClassNotFoundException | IOException | SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public String getReviews(ReviewsBeanBO bo) {
		try {
			properties = ConfigUtility.getQueryProperties();
			String query1 = properties.getProperty("$GETBOOKREVIEWS");
			String getBookReviews = query1.concat(bo.getIsbn());
			con = MyConnection.getConnection();
			statement = con.createStatement();
			results = statement.executeQuery(getBookReviews);
			StringBuilder json = new StringBuilder("[");
			while (results.next()) {
				if (results.getRow() == 1) {
					String book = "{\"isbn\":\"" + results.getString(1) + "\",\"userID\":\"" + results.getInt(2)
							+ "\",\"comment\":\"" + results.getString(3) + "\"}";
					json.append(book);
				} else {
					String book = ",{\"isbn\":\"" + results.getString(1) + "\",\"userID\":\"" + results.getInt(2)
							+ "\",\"comment\":\"" + results.getString(3) + "\"}";
					json.append(book);
				}
			}
			json.append("]");
			return json.toString();
		} catch (IOException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return "";
	}

}
