package com.bookreview.dao;

import com.bookreview.bo.BookBeanBO;

public interface BookDAO {
	public boolean addNewBook(BookBeanBO nbbo);
	public String getBooks();
	public boolean deleteBook(BookBeanBO dbbo);
}
