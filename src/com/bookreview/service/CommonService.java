package com.bookreview.service;

import com.bookreview.bo.ReviewsBeanBO;
import com.bookreview.dao.DAOFactory;
import com.bookreview.dto.ReviewsBeanDTO;

public class CommonService {
	ReviewsBeanBO bo=null;
	public String getBooks() {
		String results = DAOFactory.getBookInstance().getBooks();
		if (results != null)
			return results;
		return null;
	}
	
	public String getReviews(ReviewsBeanDTO dto){
		bo=new ReviewsBeanBO();
		bo.setComment(dto.getComment());
		bo.setIsbn(dto.getIsbn());
		bo.setRatings(dto.getRatings());
		bo.setUserID(dto.getUserId());
		String results = DAOFactory.getReviewsInstance().getReviews(bo);
		if (results != null)
			return results;
		return null;
	}

}
