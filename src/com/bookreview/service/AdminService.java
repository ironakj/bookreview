package com.bookreview.service;

import com.bookreview.bo.BookBeanBO;
import com.bookreview.dao.DAOFactory;
import com.bookreview.dto.BookBeanDTO;

public class AdminService {

	BookBeanBO bo = new BookBeanBO();

	public boolean addNewBook(BookBeanDTO dto) {
		bo.setAuthorName(dto.getAuthorName());
		bo.setBookName(dto.getBookName());
		bo.setEdition(dto.getEdition());
		bo.setIsbn(dto.getIsbn());
		bo.setPublisher(dto.getPublisher());

		if (DAOFactory.getBookInstance().addNewBook(bo))
			return true;
		return false;
	}
	
	public boolean deleteBook(BookBeanDTO dto){
		bo.setIsbn(dto.getIsbn());
		if(DAOFactory.getBookInstance().deleteBook(bo))
		   return true;
		return false;
	}
}
