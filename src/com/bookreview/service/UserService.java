package com.bookreview.service;

import com.bookreview.bo.ReviewsBeanBO;
import com.bookreview.bo.UserBeanBO;
import com.bookreview.dao.DAOFactory;
import com.bookreview.dto.ReviewsBeanDTO;
import com.bookreview.dto.UserBeanDTO;

public class UserService {
	UserBeanBO bo = null;
	ReviewsBeanBO boreview = null;

	public int addNewUser(UserBeanDTO dto) {
		bo.setUserName(dto.getUserName());
		bo.setRole(dto.getRole());
		bo.setPassword(dto.getPassword());
		return DAOFactory.getUserInstance().createUser(bo);
			
	}

	public boolean validateUser(UserBeanDTO dto) {
		bo.setUserName(dto.getUserName());
		bo.setPassword(dto.getPassword());
		if (DAOFactory.getUserInstance().selectUser(bo) == 1)
			return true;
		return false;
	}

	public boolean addReview(ReviewsBeanDTO dto) {
		boreview.setIsbn(dto.getIsbn());
		boreview.setUserID(dto.getUserId());
		boreview.setRatings(dto.getRatings());
		boreview.setComment(dto.getComment());
		if (DAOFactory.getReviewsInstance().addReview(boreview))
			return true;
		return false;

	}

	public boolean updateReview(ReviewsBeanDTO dto) {
		boreview.setIsbn(dto.getIsbn());
		boreview.setUserID(dto.getUserId());
		boreview.setRatings(dto.getRatings());
		boreview.setComment(dto.getComment());
		if (DAOFactory.getReviewsInstance().updateReview(boreview))
			return true;
		return false;
	}
}
